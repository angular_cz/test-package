'use strict';

describe('test-package', function() {

  it('should contain headline Angular.cz on index page', function() {
    browser.get('index.html')
        .then(function() {
          expect(
            element(by.css('h1')).getText()
              ).toMatch('Angular.cz');
        });
  });


});