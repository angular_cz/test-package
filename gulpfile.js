var gulp = require('gulp'),
  connect = require('gulp-connect'),
  open = require('gulp-open')
  ;

gulp.task('connect', function() {
  connect.server({
    port: 8888
  });

  gulp.src("./app/index.html")
    .pipe(open("", {url: "http://localhost:8888/app"}));   
});


gulp.task('default', ['connect']);