# Testovací balíček - www.angular.cz 

Tento balíček slouží k otestování funkčnosti npm a nodejs

## Co si nainstalovat
- git - http://git-scm.com/downloads
- aktuální verzi nodeJS - http://nodejs.org/download/

## NPM závislosti
Nodejs se nainstaloval spolu s balíčkovacím nástrojem npm. Ten použijeme pro instalaci 

 - bower - nástroj na správu javascriptových závislostí
 - gulp - task manager pro javascript
 
###Instalaci provedete spuštěním
 
```
npm install -g gulp bower
``` 


## Ověření správnosti instalace

Stáhněte tento balíček kamkoli k sobě. 

```
git clone https://bitbucket.org/angular_cz/test-package
```

Spusťte v jeho adresáři následující příkazy, žádný z nich by neměl vyhlásit chybu.

```
 1. npm install
 2. bower install
 3. gulp
```

Po spuštění *gulp* by mělo dojít k otevření localhost:8888/app, kde uvidíte informaci, že všechno funguje. 

Prima, první půlku máme za sebou. 

### Test protractoru ###

Nyní nechte spuštěný gulp a v novém terminálu ve složce projektu spusťte postupně příkazy.

```
 1. npm run update-webdriver
 2. npm run protractor
```

Žádný z příkazů by neměl hlásit chybu (první může trvat déle). Měli by jste vidět výstup testu:

```
Starting selenium standalone server...
Selenium standalone server started at http://192.168.1.17:56131/wd/hub
.

Finished in 1.515 seconds
1 test, 1 assertion, 0 failures
```

Pokud vše proběhlo podle postupu, máte správně připravené prostředí. 
V opačném případě se podívejte do sekce možné problémy a pokud tam nenaleznete řešení, kontaktujte nás.

### Možné probémy ###

####unable to connect to github.com####
pokud vidíte tuto chybovou zprávu po spuštění *bower install*
 - máte buď blokováno připojení ke githubu  - to můžete ověřit otevřením github.com v prohlížeči
 - nebo máte blokován protokol git - spusťte příkaz, který "přesměruje" protokol git po https

```
git config --global url."https://github.com/".insteadOf git@github.com:
git config --global url."https://".insteadOf git://
```

Nyní už by měl příkaz *bower install* fungovat

Pokud problémy přetrvávají, a jste uživatelem systému Windows, může zde být následující problém.

 - Nastavení výše se zapíšou do .gitconfig do domovské složky, na Windows s profilem na vzdáleném disku však může každý ze shelů hledat home jinde.
 - Pokud je toto Váš případ, zkopírujte soubor do obou umístění, na sdílenou domovskou složku a c:\users\[name]\

Další variantou je konfigurovat každý projekt samostatně (vynechat atribut --global)

####spawn child ENOENT error####
pokud při spuštění *npm run update-webdriver*, nebo *npm run protractor* vidíte tuto chybu, pravděpodobně nemáte nainstalovanou JAVU, která je nutná pro jeho spuštění. Pozor na to, že na windows musíte po instalaci javy spustit příkazovou řádku znovu, jinak se bude chyba opakovat.

####Error: Angular could not be found on the page http://localhost:8888/app/index.html : retries looking for angular exceeded####
pokud při  *npm run protractor* vidíte tuto chybu, ověřte, že na adrese http://localhost:8888/app/index.html je vidět testovací stránka. Pokud není, pravděpodobně jste vypnuly gulp po první části testu